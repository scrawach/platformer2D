using System;
using UnityEngine;

namespace Player
{
    public class PlayerInput : MonoBehaviour
    {
        private const string HorizontalAxis = "Horizontal";
        private const string VerticalAxis = "Vertical";

        public Vector2 Direction { get; private set; }
        
        private void Update() => 
            Direction = ReadInput();

        private Vector2 ReadInput() => 
            new Vector2(Input.GetAxis(HorizontalAxis), Input.GetAxis(VerticalAxis));
    }
}
